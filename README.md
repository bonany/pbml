---
layout: home
title: Processing-In-Memory Behavioral Model Library
permalink: /
---

PIM Behavioral Model Library (*PBML*) is a collection that contains typical PIM behavrioral models :wink:

PBML includes:

## Basic Behavioral Models

The following models are for behaviroal simulation (*Icarus Verilog*, *VCS*, *Modelsim*, *Incisive*, etc.).

| Item        | Reference | Source Code | 
| ----------- | :-----------: | :-----------: |
| Vector-Matrix Multiplication PIM Macro  | [\[1\]][1] | [VMM-PIM.zip](src/VMM-PIM.zip) |
| Hadamard Product PIM Macro | [\[2\]][1] | [VHP-PIM.zip](src/VHP-PIM.zip) |
| Transpose PIM Macro | [\[2\]][1] | [VT-PIM.zip](src/VT-PIM.zip) |

## FPGA Implementable Behavioral Models

The following models are fabricated with block RAMs.

| Item        | Reference | Source Code | 
| ----------- | :-----------: | :-----------: |
| Vector-Matrix Multiplication PIM Macro (FPGA Version)  |  [\[1\]][1] | [VMM-PIM_BR.zip](src/VMM-PIM_BR.zip) |
| Hadamard Product PIM Macro (FPGA Version) |  [\[2\]][2] | [VHP-PIM_BR.zip](src/abc.tar.gz) |

Note: the suffix "_BR" refers to "block RAM". 

## Citation

Plese cite this work as:

```
XXX
```

Bibtext:
```
XXX
```

## Manuals

Click left pannels on this page for detailed manuals. It includes:

* Readme
* Fundamental Circuit Operation
* Port Map
* Address Map

## License

This website is revised based on [Jekyll-Gitbook](https://github.com/sighingnow/jekyll-gitbook).
This work is open sourced under the Apache License, Version 2.0.

Copyright 2022 Bonan Yan.



[1]: https://baidu.com
[2]: https://baidu.com